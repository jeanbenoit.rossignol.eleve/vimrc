" --my own settings--
syntax on
set number
set ruler
set hlsearch
set showmatch
set autoindent
set tabstop=4
set shiftwidth=4
set noexpandtab
set list lcs=tab:\|\
set noswapfile
set backspace=indent,eol,start
set termguicolors

" --disable arrow key--
noremap <up> <nop>
noremap <down> <nop>
noremap <left> <nop>
noremap <right> <nop>

" --Plugin--
"  curl -fLo ~/.vim/autoload/plug.vim --create-dirs \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
call plug#begin('~/.vim/plugged')
	Plug 'scrooloose/nerdtree'
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'
	Plug 'sainnhe/sonokai'
	Plug 'pbondoer/vim-42header'
	Plug 'preservim/nerdtree' |
	Plug 'Xuyuanp/nerdtree-git-plugin'
	Plug 'neoclide/coc.nvim'
call plug#end()

" --sonokai-colorscheme--
let g:sonokai_style = 'shusia'
let g:sonokai_disable_italic_comment = 1
let g:airline_theme = 'sonokai'
colorscheme sonokai

" --NERDTree--
let g:NERDTreeWinSize=25

"  --coc.nvim--
"  install node to use coc.nvim
set hidden
set cmdheight=2
set encoding=utf-8
" recommended settings to prevent issue with backup --> coc.vnim issue #649
set nobackup
set nowritebackup
" set tab to go through the list
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
" set enter to to trigger completion
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
	\: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
